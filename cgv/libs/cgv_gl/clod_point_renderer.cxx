#include <algorithm>
#include <random>
#include <unordered_map>
#include <sstream>
#include "clod_point_renderer.h"

//**

namespace cgv {
	namespace render {

		clod_point_renderer& ref_clod_point_renderer(context& ctx, int ref_count_change)
		{
			static int ref_count = 0;
			static clod_point_renderer r;
			r.manage_singelton(ctx, "clod_point_renderer", ref_count, ref_count_change);
			return r;
		}

		clod_point_render_style::clod_point_render_style() {}

		bool clod_point_render_style::self_reflect(cgv::reflect::reflection_handler& rh)
		{
			return
				//rh.reflect_base(*static_cast<point_render_style*>(this)) &&
				rh.reflect_member("CLOD_factor", CLOD) &&
				rh.reflect_member("spacing", spacing) &&
				rh.reflect_member("scale", scale) &&
				rh.reflect_member("min_millimeters", min_millimeters) &&
				rh.reflect_member("point_size", pointSize) && 
				rh.reflect_member("draw_circles", draw_circles) &&
				rh.reflect_member("point_filter_delay", point_filter_delay);
		}

		void clod_point_renderer::draw_and_compute_impl(context& ctx, size_t start, size_t count)
		{
			// compute and fill marking buffer: this will introduce a latency about 8ms, running on full points 
			// todo: this is marking on the whole point cloud for now, we can ignore cutted points (eg. octree) 
			if (enable_marking) {
				marking_prog.enable(ctx);
				glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, marking_buffer); // bind marking buffer to loc 0 
				glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, marking_buffer); // passing itself as target buffer, loc 1 
				glDispatchCompute((input_buffer_num_points / 128) + 1, 1, 1); 
				glMemoryBarrier(GL_ALL_BARRIER_BITS); // synchronize
				marking_prog.disable(ctx);
			}

			if (enable_chunked_marking) {
				marking_prog.enable(ctx);
				for (int i = 0; i < num_chunks_passed_to_compute_shader; i++) {
					if (num_chunks_passed_to_compute_shader > chunked_marking_buffer.size() - 1)
						continue;
					glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, chunked_marking_buffer[i]); // input loc 
					glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, chunked_marking_buffer[i]); // output loc 
					glDispatchCompute((input_buffer_num_points / 128) + 1, 1, 1);
					glMemoryBarrier(GL_ALL_BARRIER_BITS); // synchronize
				}
				marking_prog.disable(ctx);
			}

			// ideas about undo function: 
			// keep a last marking buffer, save the difference with a structure?
			// check falling edge, last frame is trigging and current not, save to a buffer 
			// keep an action stack on gpu, we event do not have to download it, the stack will be a buffer, but its size limitation? 
			// point_id, from_selection_id, to_selection_id, but how can we mark as top? every thing is buffer? use an other int 
			// can compute shader resize a buffer? 
			// set the action stack buffer as source, marking buffer as target, undo is possible 
			// compute to reduce render buffer 
			if (true) {
				//configure shader to compute everything after one frame
				reduce_prog.set_uniform(ctx, reduce_prog.get_uniform_location(ctx, "uBatchOffset"), (int)start);
				reduce_prog.set_uniform(ctx, reduce_prog.get_uniform_location(ctx, "uBatchSize"), (int)count);
				reduce_prog.set_uniform(ctx, "frustum_extent", 1.0f);

				// reset draw parameters
				DrawParameters dp = DrawParameters();
				glNamedBufferSubData(draw_parameter_buffer, 0, sizeof(DrawParameters), &dp);
				glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

				// reduce, provides a direct visual feedback 
				reduce_prog.enable(ctx); 
				glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, marking_buffer); // reduce from marking buffer, its size is the same as the original one 
					// marking buffer may be slow here 
				glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, render_buffer); // reduced target point buffer, loc = 1
				glBindBufferBase(GL_SHADER_STORAGE_BUFFER, drawp_pos, draw_parameter_buffer); // parameters, loc = 3
				glDispatchCompute((input_buffer_num_points / 128) + 1, 1, 1); //with NVIDIA GPUs in debug mode this will spam notifications about buffer usage
				glMemoryBarrier(GL_ALL_BARRIER_BITS); // synchronize
				reduce_prog.disable(ctx);
			}

			// draw composed buffer with indirect rendering 
			draw_prog->enable(ctx);
			glBindVertexArray(vertex_array); // bind vao, bind render_buffer by default 
			glBindBuffer(GL_DRAW_INDIRECT_BUFFER, draw_parameter_buffer); // bind the updated buffer parameter (updated in compute shader )
			glDrawArraysIndirect(GL_POINTS, 0); // indirect rendering 
			glBindBuffer(GL_DRAW_INDIRECT_BUFFER,0);
			//map buffer into host address space for debugging
			//DrawParameters* device_draw_parameters = static_cast<DrawParameters*>(glMapNamedBufferRange(draw_parameter_buffer, 0, sizeof(DrawParameters), GL_MAP_READ_BIT));
			//glUnmapNamedBuffer(draw_parameter_buffer);
			glBindVertexArray(0);
			draw_prog->disable(ctx);
		}

		const render_style* clod_point_renderer::get_style_ptr() const
		{
			if (rs)
				return rs;
			if (default_render_style)
				return default_render_style;
			default_render_style = create_render_style();
			return default_render_style;
		}

		render_style* clod_point_renderer::create_render_style() const
		{
			return new clod_point_render_style();
		}

		bool clod_point_renderer::init(context& ctx)
		{
			//
			ctx_ptr = &ctx;

			//
			srand(time(NULL));

			// pre-computing 
			float y_view_angle = 45;
			pixel_extent_per_depth = (float)(2.0 * tan(0.5 * 0.0174532925199 * y_view_angle) / ctx.get_height());

			//
			if (!reduce_prog.is_created()) {
				reduce_prog.create(ctx);
				add_shader(ctx, reduce_prog, "view.glsl", cgv::render::ST_COMPUTE);
				add_shader(ctx, reduce_prog, "point_clod_filter_points.glcs", cgv::render::ST_COMPUTE);
				reduce_prog.link(ctx);
			}

			// 
			if (!marking_prog.is_created()) {
				marking_prog.create(ctx);
				add_shader(ctx, marking_prog, "point_clod_marking_points.glcs", cgv::render::ST_COMPUTE);
				marking_prog.link(ctx);
			}

			//create square drawing shader program
			if (!draw_squares_prog.is_created()) {
				draw_squares_prog.build_program(ctx, "point_clod.glpr", true);
			}

			//create circle drawing shader program
			if (!draw_circle_prog.is_created()) {
				draw_circle_prog.build_program(ctx, "point_clod_circle.glpr", true);
			}
			
			// buffer creation 
			glGenBuffers(1, &input_buffer);
			glGenBuffers(1, &render_buffer);
			glGenBuffers(1, &draw_parameter_buffer);
			glGenBuffers(1, &render_back_buffer);
			glGenBuffers(1, &marking_buffer);
			chunked_marking_buffer.resize(num_of_chunks);
			for(auto& c:chunked_marking_buffer) glGenBuffers(1, &c);

			// generate and bind vao 
			glGenVertexArrays(1, &vertex_array);
			glBindVertexArray(vertex_array);
			// bind vbo, specify data format, todo: use loc 
			glBindBuffer(GL_ARRAY_BUFFER, render_buffer);  
			glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(Point), (void*)offsetof(struct Point, p_position));//position 
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(Point), (void*)offsetof(struct Point, p_color)); // color, will be trasfered to float when passing to shader 
			glEnableVertexAttribArray(1);
			glVertexAttribIPointer(2, 1, GL_INT, sizeof(Point), (void*)offsetof(struct Point, p_selection_index));
			glEnableVertexAttribArray(2);
			//glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Point), (void*)offsetof(struct Point, p_normal)); // normal  
			//glEnableVertexAttribArray(3);
			glBindBuffer(GL_ARRAY_BUFFER, 0);
			// unbind vao
			glBindVertexArray(0);

			return draw_squares_prog.is_linked() && reduce_prog.is_linked() && marking_prog.is_linked();
		}

		bool clod_point_renderer::enable(context& ctx)
		{
			const clod_point_render_style& prs = get_style<clod_point_render_style>();
			draw_prog = (prs.draw_circles) ? &draw_circle_prog : &draw_squares_prog;

			if (!draw_prog->is_linked()) {
				return false;
			}

			if (buffers_outofdate) { // after uploaded 
				resize_buffers(ctx);
				buffers_outofdate = false;
			}

			//const clod_point_render_style& srs = get_style<clod_point_render_style>();
			vec2 screenSize(ctx.get_width(), ctx.get_height());
			//transform to model space since there is no view matrix
			vec4 pivot = inv(ctx.get_modelview_matrix())*dvec4(0.0,0.0,0.0,1.0);

			//mat4 modelview_matrix = ctx.get_modelview_matrix();
			//mat4 projection_matrix = ctx.get_projection_matrix();

			// set up point rendering uniforms 
			draw_prog->set_uniform(ctx, "CLOD" , prs.CLOD);
			draw_prog->set_uniform(ctx, "scale", prs.scale);
			draw_prog->set_uniform(ctx, "spacing", prs.spacing);
			draw_prog->set_uniform(ctx, "pointSize", prs.pointSize);
			draw_prog->set_uniform(ctx, "minMilimeters", prs.min_millimeters);
			draw_prog->set_uniform(ctx, "screenSize", screenSize);
			draw_prog->set_uniform(ctx, "pivot", pivot);

			//view.glsl uniforms are set on draw_squares_prog.enable(ctx) and  reduce_prog.enable(ctx)
			//draw_squares_prog.set_uniform(ctx, "modelview_matrix", modelview_matrix, true);
			//draw_squares_prog.set_uniform(ctx, "projection_matrix", projection_matrix, true);
			//reduce_prog.set_uniform(ctx, "modelview_matrix", modelview_matrix, true);
			//reduce_prog.set_uniform(ctx, "projection_matrix", projection_matrix, true);

			// set up reduce compute program unifoms 
			reduce_prog.set_uniform(ctx, "CLOD", prs.CLOD);
			reduce_prog.set_uniform(ctx, "scale", prs.scale); // do not do twice in drawable...
			reduce_prog.set_uniform(ctx, "spacing", prs.spacing);
			reduce_prog.set_uniform(ctx, reduce_prog.get_uniform_location(ctx, "pivot"), pivot);
			reduce_prog.set_uniform(ctx, reduce_prog.get_uniform_location(ctx, "screenSize"), screenSize);

			//general point renderer uniforms
			//draw_prog->set_uniform(ctx, "use_color_index", false);
			draw_prog->set_uniform(ctx, "pixel_extent_per_depth", pixel_extent_per_depth);
			return true;
		}

		void clod_point_renderer::clear(cgv::render::context& ctx)
		{
			reduce_prog.destruct(ctx);
			draw_squares_prog.destruct(ctx);
			draw_circle_prog.destruct(ctx);
			cgv::render::ref_clod_point_renderer(ctx, -1);
			clear_buffers(ctx);
		}

		void clod_point_renderer::draw(context& ctx, size_t start, size_t count)
		{
			draw_and_compute_impl(ctx, start, count);
		}

		bool clod_point_renderer::render(context& ctx, size_t start, size_t count)
		{
			if (enable(ctx)) {
				draw(ctx, start, count);
				return true;
			}
			return false;
		}

		void clod_point_renderer::set_render_style(const render_style& rs)
		{
			this->rs = &rs;
		}

		void clod_point_renderer::manage_singelton(context& ctx, const std::string& renderer_name, int& ref_count, int ref_count_change)
		{
			switch (ref_count_change) {
			case 1:
				if (ref_count == 0) {
					if (!init(ctx))
						ctx.error(std::string("unable to initialize ") + renderer_name + " singelton");
				}
				++ref_count;
				break;
			case 0:
				break;
			case -1:
				if (ref_count == 0)
					ctx.error(std::string("attempt to decrease reference count of ") + renderer_name + " singelton below 0");
				else {
					if (--ref_count == 0)
						clear(ctx);
				}
				break;
			default:
				ctx.error(std::string("invalid change reference count outside {-1,0,1} for ") + renderer_name + " singelton");
			}
		}

		void clod_point_renderer::add_shader(context& ctx, shader_program& prog, const std::string& sf,const cgv::render::ShaderType st)
		{
#ifndef NDEBUG
			std::cout << "add shader " << sf << '\n';
#endif // #ifdef NDEBUG
			prog.attach_file(ctx, sf, st);
#ifndef NDEBUG
			if (prog.last_error.size() > 0) {
				std::cerr << prog.last_error << '\n';
				prog.last_error = "";
			}	
#endif // #ifdef NDEBUG

		}

		///
		void clod_point_renderer::set_points_buffer_direct(const void* pnts, const size_t num_points) {
			//set_points(ctx, input_buffer_data.data(),input_buffer_data.size());
			assert(input_buffer != 0);
			input_buffer_size = num_points * sizeof(Point);
			input_buffer_num_points = num_points;
			buffers_outofdate = true; // resize required 

			// upload everything to input buffer as original copy 
			glBindBuffer(GL_SHADER_STORAGE_BUFFER, input_buffer); // bind and upload the realy large buffer, whole points 
			glBufferData(GL_SHADER_STORAGE_BUFFER, num_points * sizeof(Point), pnts, GL_STATIC_READ); // input buffer is only used to be read

			// initialize marking buffer 
			glBindBuffer(GL_SHADER_STORAGE_BUFFER, marking_buffer); // initialize marking_buffer buffer when input buffer updated 
			glBufferData(GL_SHADER_STORAGE_BUFFER, num_points * sizeof(Point), pnts, GL_DYNAMIC_DRAW); // this will be used for marking, dynamically change expected
			// state: marking buffer has been filled with original data 
		}

		// packed outside 
		void clod_point_renderer::set_points_packed(cgv::render::context& ctx, const void* pnts, const size_t num_points) {
			//set_points(ctx, input_buffer_data.data(),input_buffer_data.size());
			//assert(input_buffer != 0);
			input_buffer_size = num_points * sizeof(Point);
			input_buffer_num_points = num_points;
			buffers_outofdate = true; // resize required 

			// fill read only input buffer  
			// upload everything to input buffer as original copy 
			glBindBuffer(GL_SHADER_STORAGE_BUFFER, input_buffer); // bind and upload the realy large buffer, whole points 
			glBufferData(GL_SHADER_STORAGE_BUFFER, num_points * sizeof(Point), pnts, GL_STATIC_READ); // input buffer is only used to be read

			// fill marking buffer 
			glBindBuffer(GL_SHADER_STORAGE_BUFFER, marking_buffer); // initialize marking_buffer buffer when input buffer updated 
			glBufferData(GL_SHADER_STORAGE_BUFFER, num_points * sizeof(Point), pnts, GL_DYNAMIC_DRAW); // this will be used for marking, dynamically change expected
			
			// fill chunked marking buffer, just for testing, pnts are not set correctly 
			// it should have the same number of points, but in a chunked way 
			int num_points_normal_chunk = num_points / (chunked_marking_buffer.size() - 1);
			int num_points_last_chunk = num_points % (chunked_marking_buffer.size() - 1);
			for (int i = 0; i < chunked_marking_buffer.size(); i++) {
				int curr_num_points;
				if (i == chunked_marking_buffer.size() - 1) {
					curr_num_points = num_points_last_chunk;
				}
				else {
					curr_num_points = num_points_normal_chunk;
				}
				GLuint c = chunked_marking_buffer[i];
				glBindBuffer(GL_SHADER_STORAGE_BUFFER, c);
				glBufferData(GL_SHADER_STORAGE_BUFFER, curr_num_points * sizeof(Point), pnts, GL_DYNAMIC_DRAW);
			}
		}

		// pass vectors for a quick test, outdated 
		void clod_point_renderer::set_points(cgv::render::context& ctx, 
			const vec3* positions, const rgb8* colors, const uint8_t* lods, const size_t num_points, const unsigned stride)
		{
			// input_buffer_data is the internal points representaion 
			std::vector<Point> input_buffer_data(num_points);
			const uint8_t* pos_end = (uint8_t*)positions + (stride * num_points);
			auto input_it = input_buffer_data.begin();

			// convert points to internal format
			for (int i = 0; i < num_points; ++i) {
				input_it->position() = *positions;
				input_it->color() = *colors;
				input_it->level() = *lods;
				//input_it->p_index = i;
				//input_it->p_selection_index = 0;
				//input_it->normal() = *normals;
				++input_it;

				if (stride) {
					positions = (vec3*)((uint8_t*)positions + stride);
					colors = (rgb8*)((uint8_t*)colors + stride);
					lods += stride;
					//normals = (vec3*)((uint8_t*)normals + 1);
				}
				else {
					++positions;
					++colors;
					++lods;
				}
			}

			// 
			//set_points(ctx, input_buffer_data.data(),input_buffer_data.size());
			assert(input_buffer != 0);
			input_buffer_size = num_points * sizeof(Point);
			input_buffer_num_points = num_points;
			buffers_outofdate = true; // resize required 

			// upload everything to input buffer as original copy 
			glBindBuffer(GL_SHADER_STORAGE_BUFFER, input_buffer); // bind and upload the realy large buffer, whole points 
			glBufferData(GL_SHADER_STORAGE_BUFFER, num_points * sizeof(Point), input_buffer_data.data(), GL_STATIC_READ); // input buffer is only used to be read

			// initialize marking buffer 
			glBindBuffer(GL_SHADER_STORAGE_BUFFER, marking_buffer); // initialize marking_buffer buffer when input buffer updated 
			glBufferData(GL_SHADER_STORAGE_BUFFER, num_points * sizeof(Point), input_buffer_data.data(), GL_DYNAMIC_DRAW); // this will be used for marking, dynamically change expected
			// state: marking buffer has been filled with original data 
		}

		void clod_point_renderer::resize_buffers(context& ctx)
		{ //  fill buffers for the compute shader
			glBindBuffer(GL_SHADER_STORAGE_BUFFER, render_buffer); // resize render buffer when input buffer updated 
			glBufferData(GL_SHADER_STORAGE_BUFFER, input_buffer_size, nullptr, GL_DYNAMIC_DRAW);
			glBindBuffer(GL_SHADER_STORAGE_BUFFER, draw_parameter_buffer); // and parameters 
			glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(DrawParameters), nullptr, GL_STREAM_DRAW);
		}

		// check with re-upload every thing 
		void clod_point_renderer::download_marking_buffer(std::vector<cgv::render::clod_point_renderer::Point>* buffer_data_tobe_filled) {
			buffer_data_tobe_filled->resize(input_buffer_num_points); // just to be careful
			glBindBuffer(GL_SHADER_STORAGE_BUFFER, marking_buffer); // download from marking buffer, similar to uploading data
			glGetBufferSubData(GL_SHADER_STORAGE_BUFFER, 0, input_buffer_size, buffer_data_tobe_filled->data());
		}

		// step back once, with compute shader?  
		void clod_point_renderer::undo() {
			
		}

		// this will done with compute shader 
		void clod_point_renderer::reset_marking() {
			// todo: an other simpler way to copy data from input buffer to marking buffer? 
			//glCopyBufferSubData(input_buffer, marking_buffer, 0, 0, input_buffer_size); // doesnt work 
			
			// an otehr option is to use compute shader, use ctx_ptr
			marking_prog.enable(*ctx_ptr);
			marking_prog.set_uniform(*ctx_ptr, "is_triggering", false); // the trick is, pass everything through
			glBindVertexArray(vao_marking);
			glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, input_buffer); // passing marking buffer to loc 0 
			glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, marking_buffer); // passing itself as target buffer 
			glDispatchCompute((input_buffer_num_points / 128) + 1, 1, 1);
			// state: all points will be marked as orignal color and position 
			// point addition may hard 
			glMemoryBarrier(GL_ALL_BARRIER_BITS); // synchronize
			glBindVertexArray(0);
			marking_prog.disable(*ctx_ptr);
		}

		void clod_point_renderer::clear_buffers(const context& ctx)
		{
			glDeleteBuffers(1, &input_buffer);
			glDeleteBuffers(1, &render_buffer);
			glDeleteBuffers(1, &marking_buffer);
			glDeleteBuffers(1, &draw_parameter_buffer);
			glDeleteBuffers(1, &render_back_buffer);
			input_buffer = render_buffer = draw_parameter_buffer = render_back_buffer = 0;
		}
	}
}


#include <cgv/gui/provider.h>

namespace cgv {
	namespace gui {

		struct clod_point_render_style_gui_creator : public gui_creator {
			/// attempt to create a gui and return whether this was successful
			bool create(provider* p, const std::string& label,
				void* value_ptr, const std::string& value_type,
				const std::string& gui_type, const std::string& options, bool*) {
				if (value_type != cgv::type::info::type_name<cgv::render::clod_point_render_style>::get_name())
					return false;
				cgv::render::clod_point_render_style* rs_ptr = reinterpret_cast<cgv::render::clod_point_render_style*>(value_ptr);
				cgv::base::base* b = dynamic_cast<cgv::base::base*>(p);
				p->add_member_control(b, "CLOD factor", rs_ptr->CLOD, "value_slider", "min=0.1;max=10;ticks=true");
				p->add_member_control(b, "scale", rs_ptr->scale, "value_slider", "min=0.1;max=10;ticks=true");
				p->add_member_control(b, "point spacing", rs_ptr->spacing, "value_slider", "min=0.1;max=10;ticks=true");
				p->add_member_control(b, "point size", rs_ptr->pointSize, "value_slider", "min=0.0001;max=10;ticks=true");
				p->add_member_control(b, "min millimeters", rs_ptr->min_millimeters, "value_slider", "min=0.1;max=10;ticks=true");
				p->add_member_control(b, "draw circles", rs_ptr->draw_circles, "check");
				//p->add_member_control(b, "filter delay", rs_ptr->point_filter_delay, "value_slider", "min=0;max=10;ticks=true");
				return true;
			}
		};

		cgv::gui::gui_creator_registration<clod_point_render_style_gui_creator> cprsgc("clod_point_render_style_gui_creator");
	}
}