#include "pose_event.h"
#include "shortcut.h"

namespace cgv {
	namespace gui {

/// construct a key event from its textual description 
pose_event::pose_event(const float *_pose, const float *_last_pose, unsigned short _player_index, short _trackable_index, double _time)
	: event(EID_POSE,0,0,_time), player_index(_player_index), trackable_index(_trackable_index)
{
	std::copy(_pose, _pose + 12, pose);
	std::copy(_last_pose, _last_pose + 12, last_pose);
}

/// return player index
unsigned pose_event::get_player_index() const
{
	return player_index;
}
/// return trackable index
int pose_event::get_trackable_index() const
{
	return trackable_index;
}
/// write to stream
void pose_event::stream_out(std::ostream& os) const
{
	event::stream_out(os);
	os << "x(" << pose[0] << "," << pose[1] << "," << pose[2] << ");";
	os << "y(" << pose[3] << "," << pose[4] << "," << pose[5] << ");";
	os << "z(" << pose[6] << "," << pose[7] << "," << pose[8] << ");";
	os << "O(" << pose[9] << "," << pose[10] << "," << pose[11] << ");";
	os << "<" << player_index << ":" << trackable_index << ">";
}

/// read from stream
void pose_event::stream_in(std::istream& is)
{
	std::cerr << "not implemented" << std::endl;
}

/// return current orientation matrix
const pose_event::mat3& pose_event::get_orientation() const
{
	return reinterpret_cast<const mat3&>(pose[0]);
}

/// return current position
const pose_event::vec3& pose_event::get_position() const
{
	return reinterpret_cast<const vec3&>(pose[9]);
}
/// return current pose matrix
const pose_event::mat3x4& pose_event::get_pose_matrix() const
{
	return reinterpret_cast<const mat3x4&>(pose[0]);
}
///

void pose_event::get_pose_matrix_as_mat3(pose_event::mat3& tmppose) {
	tmppose[0] = pose[0];  tmppose[1] = pose[1];  tmppose[2] = pose[2]; 
	tmppose[3] = pose[3];  tmppose[4] = pose[4];  tmppose[5] = pose[5]; 
	tmppose[6] = pose[6];  tmppose[7] = pose[7];  tmppose[8] = pose[8]; 
}
///
void pose_event::get_pose_matrix_as_mat4(pose_event::mat4& tmppose)
{
	/*!
	- pose[0..2]  ... x-axis pointing to the right
	- pose[3..5]  ... y-axis pointing up
	- pose[6..8]  ... z-axis pointing backwards
	- pose[9..11] ... location of trackable's origin
	*/
	//float tmppose[16];
	tmppose[0] = pose[0];  tmppose[1] = pose[1];  tmppose[2] = pose[2];  tmppose[3] = 0;
	tmppose[4] = pose[3];  tmppose[5] = pose[4];  tmppose[6] = pose[5];  tmppose[7] = 0;
	tmppose[8] = pose[6];  tmppose[9] = pose[7];  tmppose[10] = pose[8]; tmppose[11] = 0;
	tmppose[12] = pose[9]; tmppose[13] = pose[10]; tmppose[14] = pose[11]; tmppose[15] = 1;

	//tmppose[0] = 1;  tmppose[1] = 0;  tmppose[2] = 0;  tmppose[3] = 0;
	//tmppose[4] = 0;  tmppose[5] = 1;  tmppose[6] = 0;  tmppose[7] = 0;
	//tmppose[8] = 0;  tmppose[9] = 0;  tmppose[10] = 1; tmppose[11] = 0;
	//tmppose[12] = 0; tmppose[13] = 0; tmppose[14] = 0; tmppose[15] = 1;

	//return reinterpret_cast<const mat4&>(tmppose[0]);
}
/// return current orientation quaternion
pose_event::quat pose_event::get_quaternion() const
{
	return quat(get_orientation());
}
/// return last orientation matrix
const pose_event::mat3& pose_event::get_last_orientation() const
{
	return reinterpret_cast<const mat3&>(last_pose[0]);
}
/// return last position
const pose_event::vec3& pose_event::get_last_position() const
{
	return reinterpret_cast<const vec3&>(last_pose[9]);
}
/// return last pose matrix
const pose_event::mat3x4& pose_event::get_last_pose_matrix() const
{
	return reinterpret_cast<const mat3x4&>(last_pose[0]);
}

/// return last orientation quaternion
pose_event::quat pose_event::get_last_quaternion() const
{
	return quat(get_last_orientation());
}

/// return difference vector from last to current position
pose_event::vec3 pose_event::get_different() const
{
	return get_position() - get_last_position();
}
/// return rotation matrix between from the last to current orientation
pose_event::mat3 pose_event::get_rotation_matrix() const
{
	return get_orientation() * transpose(get_last_orientation());
}
/// return rotation quaternion between from the last to current orientation
pose_event::quat pose_event::get_rotation_quaternion() const
{
	return quat(get_rotation_matrix());
}

	}
}
